class mycollectd {
  class { '::collectd':
    purge           => true,
    recurse         => true,
    purge_config    => true,
    minimum_version => '5.4',
  }
  class { 'collectd::plugin::logfile':
    log_level => 'warning',
    log_file => '/var/log/collected.log'
  } 
  class { 'collectd::plugin::write_kafka':
       kafka_host => 'localhost',
       kafka_port => 9092,
       topics     => {
       'mytopic'      => { 'format' => 'JSON' },
       }
     }
  package { 'collectd-write_kafka':
    ensure => 'installed',
  }
}



