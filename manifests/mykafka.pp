class mykafka {
  class { 'zookeeper':
  hostnames => [ $::fqdn ],
  }
  
  include ::zookeeper::server
  
  class { 'kafka':
    version => '1.1.0'
  }
  class { 'kafka::broker':
    config => { 'broker.id' => '0', 'zookeeper.connect' => 'localhost:2181' }
  }
}
